<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

class Tile extends Model
{
    use HasFactory;
    public const PENDING = 'PENDING';
    public const DONE = 'DONE';
    public const INVALID = 'INVALID';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected function casts()
    {
        return [
            'sections' => 'array',
            'controls' => 'array',
        ];
    }

    public function game() : BelongsTo
    {
        return $this->belongsTo(Game::class);
    }

    public function sections() : Attribute
    {
        return Attribute::make(
            get: function (string $value) : array {
                $sections = json_decode($value, true);
                // If there's an entity on the label of controls, add a link to it
                $entities = collect($this->controls[0]['entries'])
                    ->filter(fn (array $entry) => $entry['type'] === 'green')
                    ->pluck('label')
                    ->flatMap(function (string $label) {
                        $regex = '/(?<title>.+?) \((?>(?<desc>.+) - )?(?<entity>Q\d+)\)/';
                        if (!preg_match($regex, $label, $matches)) {
                            return;
                        }
                        return [Arr::only($matches, ['title', 'desc', 'entity'])];
                    })->filter()
                    ->toArray();
                foreach($entities as $info) {
                    $title = 'Opción ' . $info['entity'];
                    if (!empty(Arr::get($info, 'desc'))) {
                        $title .= ": ({$info['desc']})";
                    }
                    $sections[] = [
                        'type' => 'text',
                        'title' => $title,
                        'url' => 'https://www.wikidata.org/wiki/' . $info['entity'],
                        'text' => '',
                    ];
                }
                return $sections;
            }
        );
    }

    public function controls() : Attribute
    {
        return Attribute::make(
            get: function (string $value) : array {
                $controls = $this->getDefaultControls();
                $controls['entries'] = array_merge(
                    $controls['entries'],
                    json_decode($value, true)
                );

                if ($this->game->ready === false) {
                    // Unless the game is set as ready, replace all entities with the sandbox
                    foreach($controls['entries'] as &$entry) {
                        if (isset($entry['api_action']['entity'])) {
                            $entry['api_action']['entity'] = 'Q4115189'; // Wikidata Sandbox ID
                        } elseif (isset($entry['api_action']['claim'])) {
                            $entry['api_action']['claim'] = str_replace(
                                search: $this->qid,
                                replace: 'Q4115189',
                                subject: $entry['api_action']['claim']
                            ); // Wikidata Sandbox ID
                        }
                    }
                    unset($entry);
                }

                return [$controls];
            },
        );
    }

    public function getDefaultControls(): array
    {
        return [
            'type' => 'buttons',
            'entries' => [
                [
                    'type' => 'white',
                    'decision' => 'skip',
                    'label' => 'Siguiente (no sé)',
                ],[
                    'type' => 'blue',
                    'decision' => 'no',
                    'label' => 'Ninguna opción es correcta',
                ],
            ],
        ];
    }

    public function scopeDone(Builder $query): void
    {
        $query->where('status', self::DONE);
    }

    public function scopePending(Builder $query): void
    {
        $query->where('status', self::PENDING);
    }

    public function scopeInvalid(Builder $query): void
    {
        $query->where('status', self::INVALID);
    }

    public function scopeFromGame(Builder $query, Game $game) : void
    {
        $query->where('game_id', $game->id);
    }
}
