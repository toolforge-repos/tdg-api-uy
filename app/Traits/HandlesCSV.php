<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;

trait HandlesCSV
{
    protected function getArrayFromCSV(string $path, array $keys) : array
    {
        $data = [];
        $handle = fopen($path, 'r');
        while ($line = fgetcsv(stream: $handle)) {
            $data[] = array_combine($keys, array_map(trim(...), $line));
        }
        fclose($handle);

        return $data;
    }

    protected function getCollectionFromCSV(string $path, array $keys) : Collection
    {
        $data = collect();
        $handle = fopen($path, 'r');
        while ($line = fgetcsv(stream: $handle)) {
            $data->push(array_combine($keys, array_map(trim(...), $line)));
        }
        fclose($handle);

        return $data;
    }

    protected function getLazyCollectionFromCSV(string $path, array $keys) : LazyCollection
    {
        return LazyCollection::make(function () use ($path, $keys) {
            $handle = fopen($path, 'r');

            while ($line = fgetcsv($handle)) {
                yield array_combine($keys, array_map(trim(...), $line));
            }
            fclose($handle);
        });
    }


}