<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;

trait HandlesBNUArtists
{
    use HandlesCSV;

    private function getArtistsFromBNU() : LazyCollection
    {
        $keys = [
            'id',
            'name',
            'year_born',
            'date_born',
            'year_died',
            'date_died',
            'description',
            'country',
            'Nombre personal',
            'Datos Biog./Hist.',
            'wikidata_item',
        ];
        return $this->getLazyCollectionFromCSV(
            path: resource_path('data/catalogo_de_autoridades_BNU.csv'),
            keys: $keys
        )->skip(1);
    }

    private function getBNUReference(array $bnuArtist) : array
    {
        return [
            "snaks" => [
                "P248" => [
                    [
                        "snaktype" => "value",
                        "property" => "P248", // Afirmado en
                        "datavalue" => [
                            "type" => "wikibase-entityid",
                            "value" => [
                                "id" => "Q3308862"  // Biblioteca Nacional del Uruguay
                            ]
                        ]
                    ]
                ],
                "P12595" => [
                    [
                        "snaktype" => "value",
                        "property" => "P12595", // ID de autoridad de la BNU
                        "datavalue" => [
                            "type" => "string",
                            "value" => $bnuArtist['id']
                        ]
                    ]
                ]
            ],
            "snaks-order" => [
                "P248",
                "P12595"
            ]
        ];
    }
}