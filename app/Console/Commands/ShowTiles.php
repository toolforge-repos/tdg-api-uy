<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

use function Laravel\Prompts\info;
use function Laravel\Prompts\note;
use function Laravel\Prompts\select;

class ShowTiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:show-tiles {game?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show random tiles (optionally) from a game';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $game = Game::where('slug', $this->argument('game'))->first();

        Tile::query()
            ->when($game instanceof Game, fn ($query) => $query->fromGame($game))
            ->inRandomOrder()
            ->chunk(10, function (Collection $tiles) {
                $tiles->each(function (Tile $tile) {
                    info($tile->sections[1]['title']);
                    info('https://www.wikidata.org/wiki/' . $tile->sections[0]['q']);
                    note($tile->sections[1]['text']);

                    select(
                        'Choose one:',
                        collect($tile->controls[0]['entries'])->pluck('label')->toArray(),
                    );
                });
            });

        // $query->
    }
}
