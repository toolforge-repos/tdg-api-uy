<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use App\Traits\HandlesBNUArtists;
use App\Traits\HandlesCSV;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;

class BNUSexOrGender extends Command
{
    use HandlesCSV, HandlesBNUArtists;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:bnu-sex-or-gender {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private Game $game;
    private Collection $genders;
    private LazyCollection $bnuArtists;
    /** Sex or gender */
    private string $property = 'P21';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->game = Game::where('slug', 'bnu-ocupacion')->firstOrFail();

        $offset = (int) $this->option('offset');

        $artistsMissingGender = $this->getArtistsMissingGender();
        $this->output->progressStart($artistsMissingGender->count());

        $this->output->progressAdvance($offset);

        $this->genders = $this->getGenders();
        $this->bnuArtists = $this->getArtistsFromBNU();

        $artistsMissingGender->skip($offset)->each($this->createTile(...));
        $this->output->progressFinish();
    }

    public function createTile(array $artist, int $index) : void
    {
        $this->output->progressAdvance();
        $bnu = $this->bnuArtists->firstWhere('id', $artist['bnu_id']);

        $qid = str_replace('http://www.wikidata.org/entity/', '', $artist['url']);
        $tile = [
            'qid' => $qid,
            'property' => $this->property,
            'name' => $artist['name'],
            'game_id' => $this->game->id,
            'status' => Tile::PENDING,
            'sections' => [
                ['type' => 'item', 'q' => $qid],
                [
                    'type' => 'text',
                    'title' => $artist['name'] . ' - ¿Sexo o Género (' . $this->property . ')?',
                    'text' => $bnu['Datos Biog./Hist.'],
                ],
                // [
                //     'type' => 'wikipage',
                //     'title' => $artist['name'],
                //     'wiki' => 'eswiki',
                // ],
            ],
            'controls' => $this->genders->map(function (array $gender) use ($qid) {
                $optionQid = str_replace('http://www.wikidata.org/entity/', '', $gender['url']);
                return [
                    'type' => 'green',
                    'decision' => 'yes',
                    'label' => $gender['name'], //etiqueta que aparecerá en el botón
                    'api_action' => [
                        'action' => 'wbcreateclaim',
                        'entity' => $qid, // qid from the item to be modified
                        'property' => $this->property, // property to be modified
                        'snaktype' => 'value',
                        'value' => json_encode([
                            'entity-type' => 'item',
                            'id' => $optionQid,
                        ]),
                    ],
                ];
            })->values()->toArray(),
        ];

        Tile::updateOrCreate(['qid' => $qid, 'property' => ''], $tile);
    }

    private function getArtistsMissingGender() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/bnu-autores-sin-genero.csv'),
            keys: ['url', 'name', 'bnu_id'],
        )->skip(1);
    }

    private function getGenders() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/generos(P21).csv'),
            keys: ['url', 'name']
        );
    }
}
