<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use Illuminate\Console\Command;

class ResetGame extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:reset-game {game}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $game = Game::where('slug', (string) $this->argument('game'))->firstOrFail();

        $count = Tile::fromGame($game)->update(['status' => Tile::PENDING]);

        $this->info($count . ' tiles reset to pending');
    }
}
