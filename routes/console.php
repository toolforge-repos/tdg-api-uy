<?php

use App\Models\Game;
use App\Models\Tile;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

Artisan::command('fix-sections', function() {
    $game = Game::where('slug', 'nomenclator')->firstOrFail();
    $tiles = Tile::fromGame($game)->get();
    $tiles->each(function (Tile $tile) {
        $sections = $tile->sections;
        $sections = collect($sections)->map(function(array $section) {
            if (Str::startsWith(Arr::get($section,'url'), 'https://www.wikidata.org')) {
                return;
            }
            if (Str::startsWith(Arr::get($section,'url'), 'https://montevideo.gub.uy')) {
                $section['title'] .= ' (Nomenclátor de Calles)';
            }
            return $section;
        })->filter()
        ->toArray();
        $tile->sections = $sections;
        $tile->save();
    });
});