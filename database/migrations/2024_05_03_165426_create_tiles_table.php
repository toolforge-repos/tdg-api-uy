<?php

use App\Models\Game;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tiles', function (Blueprint $table) {
            $table->id();
            $table->string('qid')->unique();
            $table->string('name');
            $table->foreignIdFor(Game::class);
            $table->enum('status', ['PENDING', 'INVALID', 'DONE']);
            $table->string('username')->nullable()->comment('The user who completed the tile');
            $table->json('sections');
            $table->json('controls');
            $table->timestamps();

            $table->index(['status', 'game_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tiles');
    }
};
